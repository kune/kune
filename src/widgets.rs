use crate::Backend;

pub trait Widget<B: Backend>: Clone {
    fn get_native_widget(&self) -> &B::NativeWidget;
}

pub trait Button<B: Backend>: Widget<B> {
    fn set_label(&self, label: &str);
    fn add_click_listener(&self, handler: impl Fn() + 'static);
}
