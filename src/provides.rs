use crate::Backend;
use crate::Window;
use crate::widgets;

pub trait ProvidesCore: Backend + ProvidesButton {}

pub trait ProvidesButton: Backend {
    type Button: widgets::Button<Self>;

    fn create_button(&self) -> Self::Button;
}

pub trait ProvidesWindow: Backend {
    type Window: Window<Self>;

    fn create_window(&self) -> Self::Window;
}
