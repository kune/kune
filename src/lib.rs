pub mod provides;
pub mod widgets;

pub trait Backend: std::marker::Sized {
    type NativeWidget;

    fn main_loop(&self);
}

pub trait Window<B: Backend> {
    fn set_title(&self, title: &str);
    fn set_native_widget(&self, widget: &B::NativeWidget);
    fn show(&self);

    fn set_widget(&self, widget: &impl widgets::Widget<B>) {
        self.set_native_widget(widget.get_native_widget());
    }
}
